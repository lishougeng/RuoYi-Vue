package com.ruoyi.web.controller.data;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.DataSource;
import com.ruoyi.data.service.IDataSourceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 数据源定义Controller
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@RestController
@RequestMapping("/data/source")
public class DataSourceController extends BaseController
{
    @Autowired
    private IDataSourceService dataSourceService;

    /**
     * 查询数据源定义列表
     */
    @PreAuthorize("@ss.hasPermi('data:source:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataSource dataSource)
    {
        startPage();
        List<DataSource> list = dataSourceService.selectDataSourceList(dataSource);
        return getDataTable(list);
    }

    /**
     * 导出数据源定义列表
     */
    @PreAuthorize("@ss.hasPermi('data:source:export')")
    @Log(title = "数据源定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataSource dataSource)
    {
        List<DataSource> list = dataSourceService.selectDataSourceList(dataSource);
        ExcelUtil<DataSource> util = new ExcelUtil<DataSource>(DataSource.class);
        util.exportExcel(response, list, "数据源定义数据");
    }

    /**
     * 获取数据源定义详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:source:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dataSourceService.selectDataSourceById(id));
    }

    /**
     * 新增数据源定义
     */
    @PreAuthorize("@ss.hasPermi('data:source:add')")
    @Log(title = "数据源定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataSource dataSource)
    {
        return toAjax(dataSourceService.insertDataSource(dataSource));
    }

    /**
     * 修改数据源定义
     */
    @PreAuthorize("@ss.hasPermi('data:source:edit')")
    @Log(title = "数据源定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataSource dataSource)
    {
        return toAjax(dataSourceService.updateDataSource(dataSource));
    }

    /**
     * 删除数据源定义
     */
    @PreAuthorize("@ss.hasPermi('data:source:remove')")
    @Log(title = "数据源定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataSourceService.deleteDataSourceByIds(ids));
    }
}
