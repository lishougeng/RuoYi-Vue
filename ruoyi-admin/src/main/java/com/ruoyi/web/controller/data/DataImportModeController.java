package com.ruoyi.web.controller.data;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.DataImportMode;
import com.ruoyi.data.service.IDataImportModeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 数据导入方式Controller
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@RestController
@RequestMapping("/data/mode")
public class DataImportModeController extends BaseController
{
    @Autowired
    private IDataImportModeService dataImportModeService;

    /**
     * 查询数据导入方式列表
     */
    @PreAuthorize("@ss.hasPermi('data:mode:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataImportMode dataImportMode)
    {
        startPage();
        List<DataImportMode> list = dataImportModeService.selectDataImportModeList(dataImportMode);
        return getDataTable(list);
    }

    /**
     * 导出数据导入方式列表
     */
    @PreAuthorize("@ss.hasPermi('data:mode:export')")
    @Log(title = "数据导入方式", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataImportMode dataImportMode)
    {
        List<DataImportMode> list = dataImportModeService.selectDataImportModeList(dataImportMode);
        ExcelUtil<DataImportMode> util = new ExcelUtil<DataImportMode>(DataImportMode.class);
        util.exportExcel(response, list, "数据导入方式数据");
    }

    /**
     * 获取数据导入方式详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:mode:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dataImportModeService.selectDataImportModeById(id));
    }

    /**
     * 新增数据导入方式
     */
    @PreAuthorize("@ss.hasPermi('data:mode:add')")
    @Log(title = "数据导入方式", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataImportMode dataImportMode)
    {
        return toAjax(dataImportModeService.insertDataImportMode(dataImportMode));
    }

    /**
     * 修改数据导入方式
     */
    @PreAuthorize("@ss.hasPermi('data:mode:edit')")
    @Log(title = "数据导入方式", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataImportMode dataImportMode)
    {
        return toAjax(dataImportModeService.updateDataImportMode(dataImportMode));
    }

    /**
     * 删除数据导入方式
     */
    @PreAuthorize("@ss.hasPermi('data:mode:remove')")
    @Log(title = "数据导入方式", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataImportModeService.deleteDataImportModeByIds(ids));
    }
}
