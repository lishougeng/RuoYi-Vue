package com.ruoyi.web.controller.data;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.DataTargetTable;
import com.ruoyi.data.service.IDataTargetTableService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 目标定义Controller
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@RestController
@RequestMapping("/data/target/table")
public class DataTargetTableController extends BaseController
{
    @Autowired
    private IDataTargetTableService dataTargetTableService;

    /**
     * 查询目标定义列表
     */
    @PreAuthorize("@ss.hasPermi('data:target:table:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataTargetTable dataTargetTable)
    {
        startPage();
        List<DataTargetTable> list = dataTargetTableService.selectDataTargetTableList(dataTargetTable);
        return getDataTable(list);
    }

    /**
     * 导出目标定义列表
     */
    @PreAuthorize("@ss.hasPermi('data:target:table:export')")
    @Log(title = "目标定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataTargetTable dataTargetTable)
    {
        List<DataTargetTable> list = dataTargetTableService.selectDataTargetTableList(dataTargetTable);
        ExcelUtil<DataTargetTable> util = new ExcelUtil<DataTargetTable>(DataTargetTable.class);
        util.exportExcel(response, list, "目标定义数据");
    }

    /**
     * 获取目标定义详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:target:table:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dataTargetTableService.selectDataTargetTableById(id));
    }

    /**
     * 新增目标定义
     */
    @PreAuthorize("@ss.hasPermi('data:target:table:add')")
    @Log(title = "目标定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataTargetTable dataTargetTable)
    {
        return toAjax(dataTargetTableService.insertDataTargetTable(dataTargetTable));
    }

    /**
     * 修改目标定义
     */
    @PreAuthorize("@ss.hasPermi('data:target:table:edit')")
    @Log(title = "目标定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataTargetTable dataTargetTable)
    {
        return toAjax(dataTargetTableService.updateDataTargetTable(dataTargetTable));
    }

    /**
     * 删除目标定义
     */
    @PreAuthorize("@ss.hasPermi('data:target:table:remove')")
    @Log(title = "目标定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataTargetTableService.deleteDataTargetTableByIds(ids));
    }
}
