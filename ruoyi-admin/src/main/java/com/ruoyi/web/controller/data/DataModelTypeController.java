package com.ruoyi.web.controller.data;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.DataModelType;
import com.ruoyi.data.service.IDataModelTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 模型类型Controller
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@RestController
@RequestMapping("/data/type")
public class DataModelTypeController extends BaseController
{
    @Autowired
    private IDataModelTypeService dataModelTypeService;

    /**
     * 查询模型类型列表
     */
    @PreAuthorize("@ss.hasPermi('data:type:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataModelType dataModelType)
    {
        startPage();
        List<DataModelType> list = dataModelTypeService.selectDataModelTypeList(dataModelType);
        return getDataTable(list);
    }

    /**
     * 导出模型类型列表
     */
    @PreAuthorize("@ss.hasPermi('data:type:export')")
    @Log(title = "模型类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataModelType dataModelType)
    {
        List<DataModelType> list = dataModelTypeService.selectDataModelTypeList(dataModelType);
        ExcelUtil<DataModelType> util = new ExcelUtil<DataModelType>(DataModelType.class);
        util.exportExcel(response, list, "模型类型数据");
    }

    /**
     * 获取模型类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:type:query')")
    @GetMapping(value = "/{status}")
    public AjaxResult getInfo(@PathVariable("status") Long status)
    {
        return success(dataModelTypeService.selectDataModelTypeByStatus(status));
    }

    /**
     * 新增模型类型
     */
    @PreAuthorize("@ss.hasPermi('data:type:add')")
    @Log(title = "模型类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataModelType dataModelType)
    {
        return toAjax(dataModelTypeService.insertDataModelType(dataModelType));
    }

    /**
     * 修改模型类型
     */
    @PreAuthorize("@ss.hasPermi('data:type:edit')")
    @Log(title = "模型类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataModelType dataModelType)
    {
        return toAjax(dataModelTypeService.updateDataModelType(dataModelType));
    }

    /**
     * 删除模型类型
     */
    @PreAuthorize("@ss.hasPermi('data:type:remove')")
    @Log(title = "模型类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{statuss}")
    public AjaxResult remove(@PathVariable Long[] statuss)
    {
        return toAjax(dataModelTypeService.deleteDataModelTypeByStatuss(statuss));
    }
}
