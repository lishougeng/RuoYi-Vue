package com.ruoyi.web.controller.data;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.DataSourceTargetMap;
import com.ruoyi.data.service.IDataSourceTargetMapService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 映射定义Controller
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@RestController
@RequestMapping("/data/map")
public class DataSourceTargetMapController extends BaseController
{
    @Autowired
    private IDataSourceTargetMapService dataSourceTargetMapService;

    /**
     * 查询映射定义列表
     */
    @PreAuthorize("@ss.hasPermi('data:map:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataSourceTargetMap dataSourceTargetMap)
    {
        startPage();
        List<DataSourceTargetMap> list = dataSourceTargetMapService.selectDataSourceTargetMapList(dataSourceTargetMap);
        return getDataTable(list);
    }

    /**
     * 导出映射定义列表
     */
    @PreAuthorize("@ss.hasPermi('data:map:export')")
    @Log(title = "映射定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataSourceTargetMap dataSourceTargetMap)
    {
        List<DataSourceTargetMap> list = dataSourceTargetMapService.selectDataSourceTargetMapList(dataSourceTargetMap);
        ExcelUtil<DataSourceTargetMap> util = new ExcelUtil<DataSourceTargetMap>(DataSourceTargetMap.class);
        util.exportExcel(response, list, "映射定义数据");
    }

    /**
     * 获取映射定义详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:map:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dataSourceTargetMapService.selectDataSourceTargetMapById(id));
    }

    /**
     * 新增映射定义
     */
    @PreAuthorize("@ss.hasPermi('data:map:add')")
    @Log(title = "映射定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataSourceTargetMap dataSourceTargetMap)
    {
        return toAjax(dataSourceTargetMapService.insertDataSourceTargetMap(dataSourceTargetMap));
    }

    /**
     * 修改映射定义
     */
    @PreAuthorize("@ss.hasPermi('data:map:edit')")
    @Log(title = "映射定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataSourceTargetMap dataSourceTargetMap)
    {
        return toAjax(dataSourceTargetMapService.updateDataSourceTargetMap(dataSourceTargetMap));
    }

    /**
     * 删除映射定义
     */
    @PreAuthorize("@ss.hasPermi('data:map:remove')")
    @Log(title = "映射定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataSourceTargetMapService.deleteDataSourceTargetMapByIds(ids));
    }
}
