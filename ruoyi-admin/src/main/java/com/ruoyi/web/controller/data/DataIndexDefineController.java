package com.ruoyi.web.controller.data;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.data.domain.DataIndex;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.DataIndexDefine;
import com.ruoyi.data.service.IDataIndexDefineService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 指标定义Controller
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@RestController
@RequestMapping("/data/define")
public class DataIndexDefineController extends BaseController
{
    @Autowired
    private IDataIndexDefineService dataIndexDefineService;

    /**
     * 查询指标定义列表
     */
    @PreAuthorize("@ss.hasPermi('data:define:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataIndexDefine dataIndexDefine)
    {
        startPage();
        List<DataIndexDefine> list = dataIndexDefineService.selectDataIndexDefineList(dataIndexDefine);
        return getDataTable(list);
    }

    /**
     * 导出指标定义列表
     */
    @PreAuthorize("@ss.hasPermi('data:define:export')")
    @Log(title = "指标定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataIndexDefine dataIndexDefine)
    {
        List<DataIndexDefine> list = dataIndexDefineService.selectDataIndexDefineList(dataIndexDefine);
        ExcelUtil<DataIndexDefine> util = new ExcelUtil<DataIndexDefine>(DataIndexDefine.class);
        util.exportExcel(response, list, "指标定义数据");
    }

    /**
     * 获取指标定义详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:define:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dataIndexDefineService.selectDataIndexDefineById(id));
    }

    /**
     * 新增指标定义
     */
    @PreAuthorize("@ss.hasPermi('data:define:add')")
    @Log(title = "指标定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataIndexDefine dataIndexDefine)
    {
        return toAjax(dataIndexDefineService.insertDataIndexDefine(dataIndexDefine));
    }

    /**
     * 修改指标定义
     */
    @PreAuthorize("@ss.hasPermi('data:define:edit')")
    @Log(title = "指标定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataIndexDefine dataIndexDefine)
    {
        return toAjax(dataIndexDefineService.updateDataIndexDefine(dataIndexDefine));
    }

    /**
     * 删除指标定义
     */
    @PreAuthorize("@ss.hasPermi('data:define:remove')")
    @Log(title = "指标定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataIndexDefineService.deleteDataIndexDefineByIds(ids));
    }

    /**
     * 查询指标列表
     */
    @PreAuthorize("@ss.hasPermi('data:define:index')")
    @DeleteMapping("/list")
    public TableDataInfo remove(DataIndex dataIndex)
    {
        startPage();
        List<DataIndex> list = dataIndexDefineService.selectDataIndexList(dataIndex);
//        List<DataIndex> list = new ArrayList<>();
        return getDataTable(list);
    }
}
