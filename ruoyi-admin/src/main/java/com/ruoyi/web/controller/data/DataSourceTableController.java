package com.ruoyi.web.controller.data;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.DataSourceTable;
import com.ruoyi.data.service.IDataSourceTableService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 源定义Controller
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@RestController
@RequestMapping("/data/table")
public class DataSourceTableController extends BaseController
{
    @Autowired
    private IDataSourceTableService dataSourceTableService;

    /**
     * 查询源定义列表
     */
    @PreAuthorize("@ss.hasPermi('data:table:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataSourceTable dataSourceTable)
    {
        startPage();
        List<DataSourceTable> list = dataSourceTableService.selectDataSourceTableList(dataSourceTable);
        return getDataTable(list);
    }

    /**
     * 导出源定义列表
     */
    @PreAuthorize("@ss.hasPermi('data:table:export')")
    @Log(title = "源定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataSourceTable dataSourceTable)
    {
        List<DataSourceTable> list = dataSourceTableService.selectDataSourceTableList(dataSourceTable);
        ExcelUtil<DataSourceTable> util = new ExcelUtil<DataSourceTable>(DataSourceTable.class);
        util.exportExcel(response, list, "源定义数据");
    }

    /**
     * 获取源定义详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:table:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dataSourceTableService.selectDataSourceTableById(id));
    }

    /**
     * 新增源定义
     */
    @PreAuthorize("@ss.hasPermi('data:table:add')")
    @Log(title = "源定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataSourceTable dataSourceTable)
    {
        return toAjax(dataSourceTableService.insertDataSourceTable(dataSourceTable));
    }

    /**
     * 修改源定义
     */
    @PreAuthorize("@ss.hasPermi('data:table:edit')")
    @Log(title = "源定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataSourceTable dataSourceTable)
    {
        return toAjax(dataSourceTableService.updateDataSourceTable(dataSourceTable));
    }

    /**
     * 删除源定义
     */
    @PreAuthorize("@ss.hasPermi('data:table:remove')")
    @Log(title = "源定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataSourceTableService.deleteDataSourceTableByIds(ids));
    }
}
