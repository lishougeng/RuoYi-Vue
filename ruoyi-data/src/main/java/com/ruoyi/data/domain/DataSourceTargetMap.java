package com.ruoyi.data.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 映射定义对象 data_source_target_map
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public class DataSourceTargetMap extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 编号 */
    private Long id;

    /** 映射关系代码 */
    @Excel(name = "映射关系代码")
    private String code;

    /** 映射关系名称 */
    @Excel(name = "映射关系名称")
    private String name;

    /** 源字段定义 */
    @Excel(name = "源字段定义")
    private String sourceFieldsDefine;

    /** 目标表字段定义 */
    @Excel(name = "目标表字段定义")
    private String targetFieldsDefine;

    /** 指标代码 */
    @Excel(name = "指标代码")
    private String indexCode;

    /** 指标名称 */
    @Excel(name = "指标名称")
    private String indexName;

    /** 时间字段 */
    @Excel(name = "时间字段")
    private String timeField;

    /** 区划字段 */
    @Excel(name = "区划字段")
    private String areaField;

    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setSourceFieldsDefine(String sourceFieldsDefine) 
    {
        this.sourceFieldsDefine = sourceFieldsDefine;
    }

    public String getSourceFieldsDefine() 
    {
        return sourceFieldsDefine;
    }
    public void setTargetFieldsDefine(String targetFieldsDefine) 
    {
        this.targetFieldsDefine = targetFieldsDefine;
    }

    public String getTargetFieldsDefine() 
    {
        return targetFieldsDefine;
    }
    public void setIndexCode(String indexCode) 
    {
        this.indexCode = indexCode;
    }

    public String getIndexCode() 
    {
        return indexCode;
    }
    public void setIndexName(String indexName) 
    {
        this.indexName = indexName;
    }

    public String getIndexName() 
    {
        return indexName;
    }
    public void setTimeField(String timeField) 
    {
        this.timeField = timeField;
    }

    public String getTimeField() 
    {
        return timeField;
    }
    public void setAreaField(String areaField) 
    {
        this.areaField = areaField;
    }

    public String getAreaField() 
    {
        return areaField;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("sourceFieldsDefine", getSourceFieldsDefine())
            .append("targetFieldsDefine", getTargetFieldsDefine())
            .append("indexCode", getIndexCode())
            .append("indexName", getIndexName())
            .append("timeField", getTimeField())
            .append("areaField", getAreaField())
            .toString();
    }
}
