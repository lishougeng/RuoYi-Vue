package com.ruoyi.data.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 指标定义对象 data_index_define
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public class DataIndexDefine extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 编号 */
    private Long id;

    /** 指标代码 */
    @Excel(name = "指标代码")
    private String code;

    /** 指标名称 */
    @Excel(name = "指标名称")
    private String name;

    /** 算法定义 */
    @Excel(name = "算法定义")
    private String algorithmDefine;

    /** 指标类型 */
    @Excel(name = "指标类型")
    private Integer indexType;

    /** 指标类型名称 */
    @Excel(name = "指标类型名称")
    private String indexTypeName;

    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setAlgorithmDefine(String algorithmDefine) 
    {
        this.algorithmDefine = algorithmDefine;
    }

    public String getAlgorithmDefine() 
    {
        return algorithmDefine;
    }
    public void setIndexType(Integer indexType) 
    {
        this.indexType = indexType;
    }

    public Integer getIndexType() 
    {
        return indexType;
    }
    public void setIndexTypeName(String indexTypeName) 
    {
        this.indexTypeName = indexTypeName;
    }

    public String getIndexTypeName() 
    {
        return indexTypeName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("algorithmDefine", getAlgorithmDefine())
            .append("indexType", getIndexType())
            .append("indexTypeName", getIndexTypeName())
            .toString();
    }
}
