package com.ruoyi.data.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 模型类型对象 data_model_type
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public class DataModelType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 编号 */
    private Long id;

    /** 模型代码 */
    @Excel(name = "模型代码")
    private String code;

    /** 模型类型名称 */
    @Excel(name = "模型类型名称")
    private String name;

    /** 映射代码 */
    @Excel(name = "映射代码")
    private String mapCode;

    /** 公式类型 */
    @Excel(name = "公式类型")
    private Integer formulaType;

    /** 公式名称 */
    @Excel(name = "公式名称")
    private String formulaName;

    /** 指标代码 */
    @Excel(name = "指标代码")
    private String indexCode;

    /** 指标名称 */
    @Excel(name = "指标名称")
    private String indexName;

    /** 模型调用 */
    @Excel(name = "模型调用")
    private String modelCall;

    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMapCode(String mapCode) 
    {
        this.mapCode = mapCode;
    }

    public String getMapCode() 
    {
        return mapCode;
    }
    public void setFormulaType(Integer formulaType) 
    {
        this.formulaType = formulaType;
    }

    public Integer getFormulaType() 
    {
        return formulaType;
    }
    public void setFormulaName(String formulaName) 
    {
        this.formulaName = formulaName;
    }

    public String getFormulaName() 
    {
        return formulaName;
    }
    public void setIndexCode(String indexCode) 
    {
        this.indexCode = indexCode;
    }

    public String getIndexCode() 
    {
        return indexCode;
    }
    public void setIndexName(String indexName) 
    {
        this.indexName = indexName;
    }

    public String getIndexName() 
    {
        return indexName;
    }
    public void setModelCall(String modelCall) 
    {
        this.modelCall = modelCall;
    }

    public String getModelCall() 
    {
        return modelCall;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("mapCode", getMapCode())
            .append("formulaType", getFormulaType())
            .append("formulaName", getFormulaName())
            .append("indexCode", getIndexCode())
            .append("indexName", getIndexName())
            .append("modelCall", getModelCall())
            .toString();
    }
}
