package com.ruoyi.data.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Map;

public class DataIndex extends BaseEntity {

    /** 婚前医学检查率 */
    @Excel(name = "婚前医学检查率")
    private Map<String,Object> marriageExamRate;

    /** 婴儿死亡率 */
    private Map<String,Object> infantMortality;

    /** 五岁以下儿童死亡率 */
    @Excel(name = "五岁以下儿童死亡率")
    private Map<String,Object> underFiveMortality;

    /** 孕产妇死亡率 */
    @Excel(name = "孕产妇死亡率")
    private Map<String,Object> maternalMortality;

    /** 全省住院分娩率 */
    @Excel(name = "全省住院分娩率")
    private String deliveryRateofProvince;

    /** 农村孕产妇住院分娩率 */
    @Excel(name = "农村孕产妇住院分娩率")
    private Integer deliveryRateofCounty;

    /** 产前检查率 */
    @Excel(name = "产前检查率")
    private String medicalPrenatalExamRate;

    /** 孕产妇系统管理率 */
    @Excel(name = "孕产妇系统管理率")
    private String systemManagementRate;

    /** 5岁以下儿童低体重率 */
    @Excel(name = "5岁以下儿童低体重率")
    private Integer fiveChildMalnutritionRate;

    /** 7岁以下儿童健康管理率 */
    @Excel(name = "7岁以下儿童健康管理率")
    private String sevenHealthCareRate;

}
