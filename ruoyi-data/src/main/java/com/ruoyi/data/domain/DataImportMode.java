package com.ruoyi.data.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 数据导入方式对象 data_import_mode
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public class DataImportMode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 编号 */
    private Long id;

    /** 导入代码 */
    @Excel(name = "导入代码")
    private String code;

    /** 方式名称 */
    @Excel(name = "方式名称")
    private String name;

    /** 参数定义 */
    @Excel(name = "参数定义")
    private String paramDefine;

    /** 方式类型 */
    @Excel(name = "方式类型")
    private Integer modeType;

    /** 数据导入方式 */
    @Excel(name = "数据导入方式")
    private String dataImportMode;

    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setParamDefine(String paramDefine) 
    {
        this.paramDefine = paramDefine;
    }

    public String getParamDefine() 
    {
        return paramDefine;
    }
    public void setModeType(Integer modeType) 
    {
        this.modeType = modeType;
    }

    public Integer getModeType() 
    {
        return modeType;
    }
    public void setDataImportMode(String dataImportMode) 
    {
        this.dataImportMode = dataImportMode;
    }

    public String getDataImportMode() 
    {
        return dataImportMode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("paramDefine", getParamDefine())
            .append("modeType", getModeType())
            .append("dataImportMode", getDataImportMode())
            .toString();
    }
}
