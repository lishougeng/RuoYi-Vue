package com.ruoyi.data.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 数据源定义对象 data_source
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public class DataSource extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 编号 */
    private Long id;

    /** 系统代码 */
    @Excel(name = "系统代码")
    private String code;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String name;

    /** 指标依据 */
    @Excel(name = "指标依据")
    private String indexFrom;

    /** 指标算法 */
    @Excel(name = "指标算法")
    private String indexAlgorithm;

    /** 数据导入方式 */
    @Excel(name = "数据导入方式")
    private String dataImportMode;

    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIndexFrom(String indexFrom) 
    {
        this.indexFrom = indexFrom;
    }

    public String getIndexFrom() 
    {
        return indexFrom;
    }
    public void setIndexAlgorithm(String indexAlgorithm) 
    {
        this.indexAlgorithm = indexAlgorithm;
    }

    public String getIndexAlgorithm() 
    {
        return indexAlgorithm;
    }
    public void setDataImportMode(String dataImportMode) 
    {
        this.dataImportMode = dataImportMode;
    }

    public String getDataImportMode() 
    {
        return dataImportMode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("indexFrom", getIndexFrom())
            .append("indexAlgorithm", getIndexAlgorithm())
            .append("dataImportMode", getDataImportMode())
            .toString();
    }
}
