package com.ruoyi.data.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 源表定义对象 data_source_table
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public class DataSourceTable extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 编号 */
    private Long id;

    /** 源表代码 */
    @Excel(name = "源表代码")
    private String code;

    /** 源表名称 */
    @Excel(name = "源表名称")
    private String name;

    /** 字段定义 */
    @Excel(name = "字段定义")
    private String fieldsDefine;

    /** 源表类型 */
    @Excel(name = "源表类型")
    private Integer tableType;

    /** 源表类型名称 */
    @Excel(name = "源表类型名称")
    private String tableTypeName;

    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setFieldsDefine(String fieldsDefine) 
    {
        this.fieldsDefine = fieldsDefine;
    }

    public String getFieldsDefine() 
    {
        return fieldsDefine;
    }
    public void setTableType(Integer tableType) 
    {
        this.tableType = tableType;
    }

    public Integer getTableType() 
    {
        return tableType;
    }
    public void setTableTypeName(String tableTypeName) 
    {
        this.tableTypeName = tableTypeName;
    }

    public String getTableTypeName() 
    {
        return tableTypeName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("fieldsDefine", getFieldsDefine())
            .append("tableType", getTableType())
            .append("tableTypeName", getTableTypeName())
            .toString();
    }
}
