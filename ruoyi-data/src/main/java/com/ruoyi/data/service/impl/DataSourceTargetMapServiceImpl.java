package com.ruoyi.data.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.data.mapper.DataSourceTargetMapMapper;
import com.ruoyi.data.domain.DataSourceTargetMap;
import com.ruoyi.data.service.IDataSourceTargetMapService;

/**
 * 映射定义Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@Service
public class DataSourceTargetMapServiceImpl implements IDataSourceTargetMapService 
{
    @Autowired
    private DataSourceTargetMapMapper dataSourceTargetMapMapper;

    /**
     * 查询映射定义
     * 
     * @param id 映射定义主键
     * @return 映射定义
     */
    @Override
    public DataSourceTargetMap selectDataSourceTargetMapById(Long id)
    {
        return dataSourceTargetMapMapper.selectDataSourceTargetMapById(id);
    }

    /**
     * 查询映射定义列表
     * 
     * @param dataSourceTargetMap 映射定义
     * @return 映射定义
     */
    @Override
    public List<DataSourceTargetMap> selectDataSourceTargetMapList(DataSourceTargetMap dataSourceTargetMap)
    {
        return dataSourceTargetMapMapper.selectDataSourceTargetMapList(dataSourceTargetMap);
    }

    /**
     * 新增映射定义
     * 
     * @param dataSourceTargetMap 映射定义
     * @return 结果
     */
    @Override
    public int insertDataSourceTargetMap(DataSourceTargetMap dataSourceTargetMap)
    {
        dataSourceTargetMap.setCreateTime(DateUtils.getNowDate());
        return dataSourceTargetMapMapper.insertDataSourceTargetMap(dataSourceTargetMap);
    }

    /**
     * 修改映射定义
     * 
     * @param dataSourceTargetMap 映射定义
     * @return 结果
     */
    @Override
    public int updateDataSourceTargetMap(DataSourceTargetMap dataSourceTargetMap)
    {
        dataSourceTargetMap.setUpdateTime(DateUtils.getNowDate());
        return dataSourceTargetMapMapper.updateDataSourceTargetMap(dataSourceTargetMap);
    }

    /**
     * 批量删除映射定义
     * 
     * @param ids 需要删除的映射定义主键
     * @return 结果
     */
    @Override
    public int deleteDataSourceTargetMapByIds(Long[] ids)
    {
        return dataSourceTargetMapMapper.deleteDataSourceTargetMapByIds(ids);
    }

    /**
     * 删除映射定义信息
     * 
     * @param id 映射定义主键
     * @return 结果
     */
    @Override
    public int deleteDataSourceTargetMapById(Long id)
    {
        return dataSourceTargetMapMapper.deleteDataSourceTargetMapById(id);
    }
}
