package com.ruoyi.data.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.data.mapper.DataImportModeMapper;
import com.ruoyi.data.domain.DataImportMode;
import com.ruoyi.data.service.IDataImportModeService;

/**
 * 数据导入方式Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@Service
public class DataImportModeServiceImpl implements IDataImportModeService 
{
    @Autowired
    private DataImportModeMapper dataImportModeMapper;

    /**
     * 查询数据导入方式
     * 
     * @param id 数据导入方式主键
     * @return 数据导入方式
     */
    @Override
    public DataImportMode selectDataImportModeById(Long id)
    {
        return dataImportModeMapper.selectDataImportModeById(id);
    }

    /**
     * 查询数据导入方式列表
     * 
     * @param dataImportMode 数据导入方式
     * @return 数据导入方式
     */
    @Override
    public List<DataImportMode> selectDataImportModeList(DataImportMode dataImportMode)
    {
        return dataImportModeMapper.selectDataImportModeList(dataImportMode);
    }

    /**
     * 新增数据导入方式
     * 
     * @param dataImportMode 数据导入方式
     * @return 结果
     */
    @Override
    public int insertDataImportMode(DataImportMode dataImportMode)
    {
        dataImportMode.setCreateTime(DateUtils.getNowDate());
        return dataImportModeMapper.insertDataImportMode(dataImportMode);
    }

    /**
     * 修改数据导入方式
     * 
     * @param dataImportMode 数据导入方式
     * @return 结果
     */
    @Override
    public int updateDataImportMode(DataImportMode dataImportMode)
    {
        dataImportMode.setUpdateTime(DateUtils.getNowDate());
        return dataImportModeMapper.updateDataImportMode(dataImportMode);
    }

    /**
     * 批量删除数据导入方式
     * 
     * @param ids 需要删除的数据导入方式主键
     * @return 结果
     */
    @Override
    public int deleteDataImportModeByIds(Long[] ids)
    {
        return dataImportModeMapper.deleteDataImportModeByIds(ids);
    }

    /**
     * 删除数据导入方式信息
     * 
     * @param id 数据导入方式主键
     * @return 结果
     */
    @Override
    public int deleteDataImportModeById(Long id)
    {
        return dataImportModeMapper.deleteDataImportModeById(id);
    }
}
