package com.ruoyi.data.service;

import java.util.List;
import com.ruoyi.data.domain.DataSourceTargetMap;

/**
 * 映射定义Service接口
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public interface IDataSourceTargetMapService 
{
    /**
     * 查询映射定义
     * 
     * @param id 映射定义主键
     * @return 映射定义
     */
    public DataSourceTargetMap selectDataSourceTargetMapById(Long id);

    /**
     * 查询映射定义列表
     * 
     * @param dataSourceTargetMap 映射定义
     * @return 映射定义集合
     */
    public List<DataSourceTargetMap> selectDataSourceTargetMapList(DataSourceTargetMap dataSourceTargetMap);

    /**
     * 新增映射定义
     * 
     * @param dataSourceTargetMap 映射定义
     * @return 结果
     */
    public int insertDataSourceTargetMap(DataSourceTargetMap dataSourceTargetMap);

    /**
     * 修改映射定义
     * 
     * @param dataSourceTargetMap 映射定义
     * @return 结果
     */
    public int updateDataSourceTargetMap(DataSourceTargetMap dataSourceTargetMap);

    /**
     * 批量删除映射定义
     * 
     * @param ids 需要删除的映射定义主键集合
     * @return 结果
     */
    public int deleteDataSourceTargetMapByIds(Long[] ids);

    /**
     * 删除映射定义信息
     * 
     * @param id 映射定义主键
     * @return 结果
     */
    public int deleteDataSourceTargetMapById(Long id);
}
