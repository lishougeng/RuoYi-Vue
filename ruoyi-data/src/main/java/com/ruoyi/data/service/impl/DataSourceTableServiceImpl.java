package com.ruoyi.data.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.data.mapper.DataSourceTableMapper;
import com.ruoyi.data.domain.DataSourceTable;
import com.ruoyi.data.service.IDataSourceTableService;

/**
 * 源定义Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@Service
public class DataSourceTableServiceImpl implements IDataSourceTableService 
{
    @Autowired
    private DataSourceTableMapper dataSourceTableMapper;

    /**
     * 查询源定义
     * 
     * @param id 源定义主键
     * @return 源定义
     */
    @Override
    public DataSourceTable selectDataSourceTableById(Long id)
    {
        return dataSourceTableMapper.selectDataSourceTableById(id);
    }

    /**
     * 查询源定义列表
     * 
     * @param dataSourceTable 源定义
     * @return 源定义
     */
    @Override
    public List<DataSourceTable> selectDataSourceTableList(DataSourceTable dataSourceTable)
    {
        return dataSourceTableMapper.selectDataSourceTableList(dataSourceTable);
    }

    /**
     * 新增源定义
     * 
     * @param dataSourceTable 源定义
     * @return 结果
     */
    @Override
    public int insertDataSourceTable(DataSourceTable dataSourceTable)
    {
        dataSourceTable.setCreateTime(DateUtils.getNowDate());
        return dataSourceTableMapper.insertDataSourceTable(dataSourceTable);
    }

    /**
     * 修改源定义
     * 
     * @param dataSourceTable 源定义
     * @return 结果
     */
    @Override
    public int updateDataSourceTable(DataSourceTable dataSourceTable)
    {
        dataSourceTable.setUpdateTime(DateUtils.getNowDate());
        return dataSourceTableMapper.updateDataSourceTable(dataSourceTable);
    }

    /**
     * 批量删除源定义
     * 
     * @param ids 需要删除的源定义主键
     * @return 结果
     */
    @Override
    public int deleteDataSourceTableByIds(Long[] ids)
    {
        return dataSourceTableMapper.deleteDataSourceTableByIds(ids);
    }

    /**
     * 删除源定义信息
     * 
     * @param id 源定义主键
     * @return 结果
     */
    @Override
    public int deleteDataSourceTableById(Long id)
    {
        return dataSourceTableMapper.deleteDataSourceTableById(id);
    }
}
