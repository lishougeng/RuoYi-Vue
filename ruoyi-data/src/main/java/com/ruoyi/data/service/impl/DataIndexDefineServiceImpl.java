package com.ruoyi.data.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.data.domain.DataIndex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.data.mapper.DataIndexDefineMapper;
import com.ruoyi.data.domain.DataIndexDefine;
import com.ruoyi.data.service.IDataIndexDefineService;

/**
 * 指标定义Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@Service
public class DataIndexDefineServiceImpl implements IDataIndexDefineService 
{
    @Autowired
    private DataIndexDefineMapper dataIndexDefineMapper;

    /**
     * 查询指标定义
     * 
     * @param id 指标定义主键
     * @return 指标定义
     */
    @Override
    public DataIndexDefine selectDataIndexDefineById(Long id)
    {
        return dataIndexDefineMapper.selectDataIndexDefineById(id);
    }

    /**
     * 查询指标定义列表
     * 
     * @param dataIndexDefine 指标定义
     * @return 指标定义
     */
    @Override
    public List<DataIndexDefine> selectDataIndexDefineList(DataIndexDefine dataIndexDefine)
    {
        return dataIndexDefineMapper.selectDataIndexDefineList(dataIndexDefine);
    }

    /**
     * 新增指标定义
     * 
     * @param dataIndexDefine 指标定义
     * @return 结果
     */
    @Override
    public int insertDataIndexDefine(DataIndexDefine dataIndexDefine)
    {
        dataIndexDefine.setCreateTime(DateUtils.getNowDate());
        return dataIndexDefineMapper.insertDataIndexDefine(dataIndexDefine);
    }

    /**
     * 修改指标定义
     * 
     * @param dataIndexDefine 指标定义
     * @return 结果
     */
    @Override
    public int updateDataIndexDefine(DataIndexDefine dataIndexDefine)
    {
        dataIndexDefine.setUpdateTime(DateUtils.getNowDate());
        return dataIndexDefineMapper.updateDataIndexDefine(dataIndexDefine);
    }

    /**
     * 批量删除指标定义
     * 
     * @param ids 需要删除的指标定义主键
     * @return 结果
     */
    @Override
    public int deleteDataIndexDefineByIds(Long[] ids)
    {
        return dataIndexDefineMapper.deleteDataIndexDefineByIds(ids);
    }

    /**
     * 删除指标定义信息
     * 
     * @param id 指标定义主键
     * @return 结果
     */
    @Override
    public int deleteDataIndexDefineById(Long id)
    {
        return dataIndexDefineMapper.deleteDataIndexDefineById(id);
    }

    @Override
    public List<DataIndex> selectDataIndexList(DataIndex dataIndex) {
        //婚前医学检查率
        Map<String, Object> map = dataIndexDefineMapper.selectWomanPremaritalHealthInfo(dataIndex);
        return dataIndexDefineMapper.selectDataIndexList(dataIndex);
    }
}
