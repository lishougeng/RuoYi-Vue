package com.ruoyi.data.service;

import java.util.List;
import com.ruoyi.data.domain.DataSourceTable;

/**
 * 源定义Service接口
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public interface IDataSourceTableService 
{
    /**
     * 查询源定义
     * 
     * @param id 源定义主键
     * @return 源定义
     */
    public DataSourceTable selectDataSourceTableById(Long id);

    /**
     * 查询源定义列表
     * 
     * @param dataSourceTable 源定义
     * @return 源定义集合
     */
    public List<DataSourceTable> selectDataSourceTableList(DataSourceTable dataSourceTable);

    /**
     * 新增源定义
     * 
     * @param dataSourceTable 源定义
     * @return 结果
     */
    public int insertDataSourceTable(DataSourceTable dataSourceTable);

    /**
     * 修改源定义
     * 
     * @param dataSourceTable 源定义
     * @return 结果
     */
    public int updateDataSourceTable(DataSourceTable dataSourceTable);

    /**
     * 批量删除源定义
     * 
     * @param ids 需要删除的源定义主键集合
     * @return 结果
     */
    public int deleteDataSourceTableByIds(Long[] ids);

    /**
     * 删除源定义信息
     * 
     * @param id 源定义主键
     * @return 结果
     */
    public int deleteDataSourceTableById(Long id);
}
