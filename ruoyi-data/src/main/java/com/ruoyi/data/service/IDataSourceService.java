package com.ruoyi.data.service;

import java.util.List;
import com.ruoyi.data.domain.DataSource;

/**
 * 数据源定义Service接口
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public interface IDataSourceService 
{
    /**
     * 查询数据源定义
     * 
     * @param id 数据源定义主键
     * @return 数据源定义
     */
    public DataSource selectDataSourceById(Long id);

    /**
     * 查询数据源定义列表
     * 
     * @param dataSource 数据源定义
     * @return 数据源定义集合
     */
    public List<DataSource> selectDataSourceList(DataSource dataSource);

    /**
     * 新增数据源定义
     * 
     * @param dataSource 数据源定义
     * @return 结果
     */
    public int insertDataSource(DataSource dataSource);

    /**
     * 修改数据源定义
     * 
     * @param dataSource 数据源定义
     * @return 结果
     */
    public int updateDataSource(DataSource dataSource);

    /**
     * 批量删除数据源定义
     * 
     * @param ids 需要删除的数据源定义主键集合
     * @return 结果
     */
    public int deleteDataSourceByIds(Long[] ids);

    /**
     * 删除数据源定义信息
     * 
     * @param id 数据源定义主键
     * @return 结果
     */
    public int deleteDataSourceById(Long id);
}
