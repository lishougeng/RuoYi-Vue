package com.ruoyi.data.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.data.mapper.DataTargetTableMapper;
import com.ruoyi.data.domain.DataTargetTable;
import com.ruoyi.data.service.IDataTargetTableService;

/**
 * 目标定义Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@Service
public class DataTargetTableServiceImpl implements IDataTargetTableService 
{
    @Autowired
    private DataTargetTableMapper dataTargetTableMapper;

    /**
     * 查询目标定义
     * 
     * @param id 目标定义主键
     * @return 目标定义
     */
    @Override
    public DataTargetTable selectDataTargetTableById(Long id)
    {
        return dataTargetTableMapper.selectDataTargetTableById(id);
    }

    /**
     * 查询目标定义列表
     * 
     * @param dataTargetTable 目标定义
     * @return 目标定义
     */
    @Override
    public List<DataTargetTable> selectDataTargetTableList(DataTargetTable dataTargetTable)
    {
        return dataTargetTableMapper.selectDataTargetTableList(dataTargetTable);
    }

    /**
     * 新增目标定义
     * 
     * @param dataTargetTable 目标定义
     * @return 结果
     */
    @Override
    public int insertDataTargetTable(DataTargetTable dataTargetTable)
    {
        dataTargetTable.setCreateTime(DateUtils.getNowDate());
        return dataTargetTableMapper.insertDataTargetTable(dataTargetTable);
    }

    /**
     * 修改目标定义
     * 
     * @param dataTargetTable 目标定义
     * @return 结果
     */
    @Override
    public int updateDataTargetTable(DataTargetTable dataTargetTable)
    {
        dataTargetTable.setUpdateTime(DateUtils.getNowDate());
        return dataTargetTableMapper.updateDataTargetTable(dataTargetTable);
    }

    /**
     * 批量删除目标定义
     * 
     * @param ids 需要删除的目标定义主键
     * @return 结果
     */
    @Override
    public int deleteDataTargetTableByIds(Long[] ids)
    {
        return dataTargetTableMapper.deleteDataTargetTableByIds(ids);
    }

    /**
     * 删除目标定义信息
     * 
     * @param id 目标定义主键
     * @return 结果
     */
    @Override
    public int deleteDataTargetTableById(Long id)
    {
        return dataTargetTableMapper.deleteDataTargetTableById(id);
    }
}
