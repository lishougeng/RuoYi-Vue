package com.ruoyi.data.service;

import java.util.List;
import com.ruoyi.data.domain.DataTargetTable;

/**
 * 目标定义Service接口
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public interface IDataTargetTableService 
{
    /**
     * 查询目标定义
     * 
     * @param id 目标定义主键
     * @return 目标定义
     */
    public DataTargetTable selectDataTargetTableById(Long id);

    /**
     * 查询目标定义列表
     * 
     * @param dataTargetTable 目标定义
     * @return 目标定义集合
     */
    public List<DataTargetTable> selectDataTargetTableList(DataTargetTable dataTargetTable);

    /**
     * 新增目标定义
     * 
     * @param dataTargetTable 目标定义
     * @return 结果
     */
    public int insertDataTargetTable(DataTargetTable dataTargetTable);

    /**
     * 修改目标定义
     * 
     * @param dataTargetTable 目标定义
     * @return 结果
     */
    public int updateDataTargetTable(DataTargetTable dataTargetTable);

    /**
     * 批量删除目标定义
     * 
     * @param ids 需要删除的目标定义主键集合
     * @return 结果
     */
    public int deleteDataTargetTableByIds(Long[] ids);

    /**
     * 删除目标定义信息
     * 
     * @param id 目标定义主键
     * @return 结果
     */
    public int deleteDataTargetTableById(Long id);
}
