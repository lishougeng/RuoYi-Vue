package com.ruoyi.data.service;

import java.util.List;

import com.ruoyi.data.domain.DataIndex;
import com.ruoyi.data.domain.DataIndexDefine;

/**
 * 指标定义Service接口
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public interface IDataIndexDefineService 
{
    /**
     * 查询指标定义
     * 
     * @param id 指标定义主键
     * @return 指标定义
     */
    public DataIndexDefine selectDataIndexDefineById(Long id);

    /**
     * 查询指标定义列表
     * 
     * @param dataIndexDefine 指标定义
     * @return 指标定义集合
     */
    public List<DataIndexDefine> selectDataIndexDefineList(DataIndexDefine dataIndexDefine);

    /**
     * 新增指标定义
     * 
     * @param dataIndexDefine 指标定义
     * @return 结果
     */
    public int insertDataIndexDefine(DataIndexDefine dataIndexDefine);

    /**
     * 修改指标定义
     * 
     * @param dataIndexDefine 指标定义
     * @return 结果
     */
    public int updateDataIndexDefine(DataIndexDefine dataIndexDefine);

    /**
     * 批量删除指标定义
     * 
     * @param ids 需要删除的指标定义主键集合
     * @return 结果
     */
    public int deleteDataIndexDefineByIds(Long[] ids);

    /**
     * 删除指标定义信息
     * 
     * @param id 指标定义主键
     * @return 结果
     */
    public int deleteDataIndexDefineById(Long id);

    /**
     * 指标数据列表查询
     * @param dataIndex
     * @return
     */
    List<DataIndex> selectDataIndexList(DataIndex dataIndex);
}
