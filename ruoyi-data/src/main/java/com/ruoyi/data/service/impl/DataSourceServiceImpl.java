package com.ruoyi.data.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.data.mapper.DataSourceMapper;
import com.ruoyi.data.domain.DataSource;
import com.ruoyi.data.service.IDataSourceService;

/**
 * 数据源定义Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@Service
public class DataSourceServiceImpl implements IDataSourceService 
{
    @Autowired
    private DataSourceMapper dataSourceMapper;

    /**
     * 查询数据源定义
     * 
     * @param id 数据源定义主键
     * @return 数据源定义
     */
    @Override
    public DataSource selectDataSourceById(Long id)
    {
        return dataSourceMapper.selectDataSourceById(id);
    }

    /**
     * 查询数据源定义列表
     * 
     * @param dataSource 数据源定义
     * @return 数据源定义
     */
    @Override
    public List<DataSource> selectDataSourceList(DataSource dataSource)
    {
        return dataSourceMapper.selectDataSourceList(dataSource);
    }

    /**
     * 新增数据源定义
     * 
     * @param dataSource 数据源定义
     * @return 结果
     */
    @Override
    public int insertDataSource(DataSource dataSource)
    {
        dataSource.setCreateTime(DateUtils.getNowDate());
        return dataSourceMapper.insertDataSource(dataSource);
    }

    /**
     * 修改数据源定义
     * 
     * @param dataSource 数据源定义
     * @return 结果
     */
    @Override
    public int updateDataSource(DataSource dataSource)
    {
        dataSource.setUpdateTime(DateUtils.getNowDate());
        return dataSourceMapper.updateDataSource(dataSource);
    }

    /**
     * 批量删除数据源定义
     * 
     * @param ids 需要删除的数据源定义主键
     * @return 结果
     */
    @Override
    public int deleteDataSourceByIds(Long[] ids)
    {
        return dataSourceMapper.deleteDataSourceByIds(ids);
    }

    /**
     * 删除数据源定义信息
     * 
     * @param id 数据源定义主键
     * @return 结果
     */
    @Override
    public int deleteDataSourceById(Long id)
    {
        return dataSourceMapper.deleteDataSourceById(id);
    }
}
