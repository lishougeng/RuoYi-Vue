package com.ruoyi.data.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.data.mapper.DataModelTypeMapper;
import com.ruoyi.data.domain.DataModelType;
import com.ruoyi.data.service.IDataModelTypeService;

/**
 * 模型类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
@Service
public class DataModelTypeServiceImpl implements IDataModelTypeService 
{
    @Autowired
    private DataModelTypeMapper dataModelTypeMapper;

    /**
     * 查询模型类型
     * 
     * @param status 模型类型主键
     * @return 模型类型
     */
    @Override
    public DataModelType selectDataModelTypeByStatus(Long status)
    {
        return dataModelTypeMapper.selectDataModelTypeByStatus(status);
    }

    /**
     * 查询模型类型列表
     * 
     * @param dataModelType 模型类型
     * @return 模型类型
     */
    @Override
    public List<DataModelType> selectDataModelTypeList(DataModelType dataModelType)
    {
        return dataModelTypeMapper.selectDataModelTypeList(dataModelType);
    }

    /**
     * 新增模型类型
     * 
     * @param dataModelType 模型类型
     * @return 结果
     */
    @Override
    public int insertDataModelType(DataModelType dataModelType)
    {
        dataModelType.setCreateTime(DateUtils.getNowDate());
        return dataModelTypeMapper.insertDataModelType(dataModelType);
    }

    /**
     * 修改模型类型
     * 
     * @param dataModelType 模型类型
     * @return 结果
     */
    @Override
    public int updateDataModelType(DataModelType dataModelType)
    {
        dataModelType.setUpdateTime(DateUtils.getNowDate());
        return dataModelTypeMapper.updateDataModelType(dataModelType);
    }

    /**
     * 批量删除模型类型
     * 
     * @param statuss 需要删除的模型类型主键
     * @return 结果
     */
    @Override
    public int deleteDataModelTypeByStatuss(Long[] statuss)
    {
        return dataModelTypeMapper.deleteDataModelTypeByStatuss(statuss);
    }

    /**
     * 删除模型类型信息
     * 
     * @param status 模型类型主键
     * @return 结果
     */
    @Override
    public int deleteDataModelTypeByStatus(Long status)
    {
        return dataModelTypeMapper.deleteDataModelTypeByStatus(status);
    }
}
