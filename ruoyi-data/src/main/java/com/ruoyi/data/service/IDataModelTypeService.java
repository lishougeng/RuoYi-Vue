package com.ruoyi.data.service;

import java.util.List;
import com.ruoyi.data.domain.DataModelType;

/**
 * 模型类型Service接口
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public interface IDataModelTypeService 
{
    /**
     * 查询模型类型
     * 
     * @param status 模型类型主键
     * @return 模型类型
     */
    public DataModelType selectDataModelTypeByStatus(Long status);

    /**
     * 查询模型类型列表
     * 
     * @param dataModelType 模型类型
     * @return 模型类型集合
     */
    public List<DataModelType> selectDataModelTypeList(DataModelType dataModelType);

    /**
     * 新增模型类型
     * 
     * @param dataModelType 模型类型
     * @return 结果
     */
    public int insertDataModelType(DataModelType dataModelType);

    /**
     * 修改模型类型
     * 
     * @param dataModelType 模型类型
     * @return 结果
     */
    public int updateDataModelType(DataModelType dataModelType);

    /**
     * 批量删除模型类型
     * 
     * @param statuss 需要删除的模型类型主键集合
     * @return 结果
     */
    public int deleteDataModelTypeByStatuss(Long[] statuss);

    /**
     * 删除模型类型信息
     * 
     * @param status 模型类型主键
     * @return 结果
     */
    public int deleteDataModelTypeByStatus(Long status);
}
