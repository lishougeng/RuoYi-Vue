package com.ruoyi.data.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.data.domain.DataIndex;
import com.ruoyi.data.domain.DataIndexDefine;

/**
 * 指标定义Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public interface DataIndexDefineMapper 
{
    /**
     * 查询指标定义
     * 
     * @param id 指标定义主键
     * @return 指标定义
     */
    public DataIndexDefine selectDataIndexDefineById(Long id);

    /**
     * 查询指标定义列表
     * 
     * @param dataIndexDefine 指标定义
     * @return 指标定义集合
     */
    public List<DataIndexDefine> selectDataIndexDefineList(DataIndexDefine dataIndexDefine);

    /**
     * 新增指标定义
     * 
     * @param dataIndexDefine 指标定义
     * @return 结果
     */
    public int insertDataIndexDefine(DataIndexDefine dataIndexDefine);

    /**
     * 修改指标定义
     * 
     * @param dataIndexDefine 指标定义
     * @return 结果
     */
    public int updateDataIndexDefine(DataIndexDefine dataIndexDefine);

    /**
     * 删除指标定义
     * 
     * @param id 指标定义主键
     * @return 结果
     */
    public int deleteDataIndexDefineById(Long id);

    /**
     * 批量删除指标定义
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDataIndexDefineByIds(Long[] ids);

    List<DataIndex> selectDataIndexList(DataIndex dataIndex);

    Map<String ,Object> selectWomanPremaritalHealthInfo(DataIndex dataIndex);
}
