package com.ruoyi.data.mapper;

import java.util.List;
import com.ruoyi.data.domain.DataImportMode;

/**
 * 数据导入方式Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public interface DataImportModeMapper 
{
    /**
     * 查询数据导入方式
     * 
     * @param id 数据导入方式主键
     * @return 数据导入方式
     */
    public DataImportMode selectDataImportModeById(Long id);

    /**
     * 查询数据导入方式列表
     * 
     * @param dataImportMode 数据导入方式
     * @return 数据导入方式集合
     */
    public List<DataImportMode> selectDataImportModeList(DataImportMode dataImportMode);

    /**
     * 新增数据导入方式
     * 
     * @param dataImportMode 数据导入方式
     * @return 结果
     */
    public int insertDataImportMode(DataImportMode dataImportMode);

    /**
     * 修改数据导入方式
     * 
     * @param dataImportMode 数据导入方式
     * @return 结果
     */
    public int updateDataImportMode(DataImportMode dataImportMode);

    /**
     * 删除数据导入方式
     * 
     * @param id 数据导入方式主键
     * @return 结果
     */
    public int deleteDataImportModeById(Long id);

    /**
     * 批量删除数据导入方式
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDataImportModeByIds(Long[] ids);
}
