package com.ruoyi.data.mapper;

import java.util.List;
import com.ruoyi.data.domain.DataTargetTable;

/**
 * 目标定义Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public interface DataTargetTableMapper 
{
    /**
     * 查询目标定义
     * 
     * @param id 目标定义主键
     * @return 目标定义
     */
    public DataTargetTable selectDataTargetTableById(Long id);

    /**
     * 查询目标定义列表
     * 
     * @param dataTargetTable 目标定义
     * @return 目标定义集合
     */
    public List<DataTargetTable> selectDataTargetTableList(DataTargetTable dataTargetTable);

    /**
     * 新增目标定义
     * 
     * @param dataTargetTable 目标定义
     * @return 结果
     */
    public int insertDataTargetTable(DataTargetTable dataTargetTable);

    /**
     * 修改目标定义
     * 
     * @param dataTargetTable 目标定义
     * @return 结果
     */
    public int updateDataTargetTable(DataTargetTable dataTargetTable);

    /**
     * 删除目标定义
     * 
     * @param id 目标定义主键
     * @return 结果
     */
    public int deleteDataTargetTableById(Long id);

    /**
     * 批量删除目标定义
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDataTargetTableByIds(Long[] ids);
}
