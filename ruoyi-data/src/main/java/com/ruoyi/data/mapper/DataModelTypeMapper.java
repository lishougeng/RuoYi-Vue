package com.ruoyi.data.mapper;

import java.util.List;
import com.ruoyi.data.domain.DataModelType;

/**
 * 模型类型Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-10
 */
public interface DataModelTypeMapper 
{
    /**
     * 查询模型类型
     * 
     * @param status 模型类型主键
     * @return 模型类型
     */
    public DataModelType selectDataModelTypeByStatus(Long status);

    /**
     * 查询模型类型列表
     * 
     * @param dataModelType 模型类型
     * @return 模型类型集合
     */
    public List<DataModelType> selectDataModelTypeList(DataModelType dataModelType);

    /**
     * 新增模型类型
     * 
     * @param dataModelType 模型类型
     * @return 结果
     */
    public int insertDataModelType(DataModelType dataModelType);

    /**
     * 修改模型类型
     * 
     * @param dataModelType 模型类型
     * @return 结果
     */
    public int updateDataModelType(DataModelType dataModelType);

    /**
     * 删除模型类型
     * 
     * @param status 模型类型主键
     * @return 结果
     */
    public int deleteDataModelTypeByStatus(Long status);

    /**
     * 批量删除模型类型
     * 
     * @param statuss 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDataModelTypeByStatuss(Long[] statuss);
}
